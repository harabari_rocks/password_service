# Password service

### A simple service to anonymously store passwords and tokens

- Why use a service dedicated to storing just passwords and tokens?

    The interest here is to store passwords and tokens in a different database than user information. This allows to avoid leaking both users and their passowrds in case of a hack, and also makes it impossible to reverse link a password/token to a user because we use SHA-1 as id.

- Why there is no security configured?

    This service is intended to be used internally only, by a dedicated user service. There will be no outside access to this service. Also in case TLS encription of the traffik is desired, external tools such as ISTIO can be used for that
